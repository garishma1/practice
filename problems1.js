
const arrayOfObjects = [
    { 
       id: 1, 
       name: 'Alice', 
       age: 30,
       email: 'alice@example.com',
       city: 'New York',
       country: 'USA',
       hobbies: ['reading', 'painting'],
       isStudent: false
    },
    { 
       id: 2, 
       name: 'Bob', 
       age: 25,
       email: 'bob@example.com',
       city: 'London',
       country: 'UK',
       hobbies: ['playing guitar', 'hiking'],
       isStudent: true
    },
    { 
       id: 3, 
       name: 'Charlie', 
       age: 35,
       email: 'charlie@example.com',
       city: 'Paris',
       country: 'France',
       hobbies: ['cooking', 'gardening'],
       isStudent: false
    },
    { 
       id: 4, 
       name: 'David', 
       age: 28,
       email: 'david@example.com',
       city: 'Berlin',
       country: 'Germany',
       hobbies: ['photography', 'traveling'],
       isStudent: true
    },
    { 
       id: 5, 
       name: 'Eve', 
       age: 32,
       email: 'eve@example.com',
       city: 'Sydney',
       country: 'Australia',
       hobbies: ['yoga', 'surfing'],
       isStudent: false
    },
    { 
       id: 6, 
       name: 'Frank', 
       age: 33,
       email: 'frank@example.com',
       city: 'Los Angeles',
       country: 'USA',
       hobbies: ['playing basketball', 'reading'],
       isStudent: true
    },
    { 
       id: 7, 
       name: 'Grace', 
       age: 29,
       email: 'grace@example.com',
       city: 'Toronto',
       country: 'Canada',
       hobbies: ['painting', 'running'],
       isStudent: false
    },
    { 
       id: 8, 
       name: 'Hannah', 
       age: 31,
       email: 'hannah@example.com',
       city: 'Melbourne',
       country: 'Australia',
       hobbies: ['writing', 'knitting'],
       isStudent: true
    },
    { 
       id: 9, 
       name: 'Ivy', 
       age: 27,
       email: 'ivy@example.com',
       city: 'Tokyo',
       country: 'Japan',
       hobbies: ['playing piano', 'cooking'],
       isStudent: false
    },
    { 
       id: 10, 
       name: 'Jack', 
       age: 34,
       email: 'jack@example.com',
       city: 'Mumbai',
       country: 'India',
       hobbies: ['playing cricket', 'watching movies'],
       isStudent: true
    }
  ];
  
  
  //    Given the dataset of individuals, write a function that accesses and returns the email addresses of all individuals.

       function emailAddresses(data){
        let emailAddresses = data.map((element)=>{
            return element.email
        })
        return emailAddresses
        
       }
       console.log(emailAddresses(arrayOfObjects))
  
  //    Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.
      
  //function hobbiesOfindividuals(data,age){
    //let hobbies = data.reduce((acc,cv)=>{
        //acc[cv.age]
        //acc.push(cv.hobbies)
            //return acc
   // },0)
    
    
  //}
  //console.log(hobbiesOfindividuals(arrayOfObjects,30))

  
  //    Create a function that extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia.
  
  //    Write a function that accesses and logs the name and city of the individual at the index position 3 in the dataset.
  
  //    Implement a loop to access and print the ages of all individuals in the dataset.

   function agesofall(data){
    let ages={}
     data.map((element)=>{
       ages[element.name]=element.age
        
    })
    return ages
   }
   console.log(agesofall(arrayOfObjects))
  
  //    Create a function to retrieve and display the first hobby of each individual in the dataset.

    function firsthobby(data){
        let hobby = {}
        data.map((element)=>{
            hobby[element.name]=element.hobbies[0]
        })
        return hobby
    }
  console.log(firsthobby(arrayOfObjects))
  //    Write a function that accesses and prints the names and email addresses of individuals aged 25.

  //function namesandemail(data){
    //let names ={}
     //data.filter((element)=>{
       // if(element.age==25)
       // names[element.name]
        //names[element.email]
   // })
    //return names

  //}
  //console.log(namesandemail(arrayOfObjects))
  
  //    Implement a loop to access and log the city and country of each individual in the dataset.

  function cityandcountry(data){
    let cityandcountryofeach = []
    data.map((element)=>{
         cityandcountryofeach[element.name]={}
         cityandcountryofeach[element.name]["city"]=element.city
         cityandcountryofeach[element.name]["country"]=element.country
    })
    return cityandcountryofeach
  }
  console.log(cityandcountry(arrayOfObjects))
